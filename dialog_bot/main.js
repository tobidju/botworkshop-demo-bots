/*
Dies ist ein einfacher Dialog-Bot.
Alle Nachrichten werden an einen Google Assistant weitergeleitet.
Die Antworten des Google Assistant werden wiederum an den Chat weitergeleitet.
Einige Gerätespezifische Befehle ("lauter", "leiser", "ausschalten", etc.)
funktionieren nicht.
*/
process.env["NTBA_FIX_319"] = 1;
const TelegramBot = require('node-telegram-bot-api') // die Funktionalität für Telegram-Bots
const path = require('path');
const GoogleAssistant = require('google-assistant'); // die Funktionalität für Google Assistant

// der Telegram token vom @BotFather
const token = 'todo'

// ein neuer Bot
const bot = new TelegramBot(token, {polling: true})

// google assistant Konfiguration
const config = {
    auth: {
      keyFilePath: path.resolve(__dirname, 'replace_this_with_your.json'),
      // Speicher für neue Token
      // Erstellt eine neue Datei, wenn noch nicht vorhanden
      savedTokensPath: path.resolve(__dirname, 'tokens.json'),
    },
    // Parameter einer Konversation
    conversation: {
      lang: 'de-DE', // Sprache für Input/Output
      deviceModelId: 'todo', // Registriertes Gerät
      deviceId: 'todo',
      isNew: false, // setze die alte Konversation fort
      screen: {
        isOn: true, // das Ergebnis soll auf einem Bildschirm ausgegeben werden
      },
    },
};
// ein neuer Google Assistant
const assistant = new GoogleAssistant(config.auth);


const waitForInput = () => {
    // wenn eine message eintrifft, dann starte eine Konversation mit dem Assistant
    bot.on("message", (msg) => {

        const chatId = msg.chat.id

        // erstelle eine Konversation
        const startConversation = (conversation) => {
            conversation
                // sende die antwort an den/die Nutzerin zurück
                .on('response', text => {
                    console.log('Received: ' + msg.text)
                    // wenn die Nachricht nicht verstanden wurde, dann sende eine ensprechende Nachricht
                    if(text === "") text = "Das habe ich leider nicht verstanden."
                    console.log('Assistant Response:', text)
                    bot.sendMessage(chatId, text)
                })
                .on('debug-info', info => console.log('Debug Info:', info))
                // beende die Konversation
                .on('ended', (error, continueConversation) => {
                    if (error) {
                        console.log('Conversation Ended Error:', error);
                    } else {
                        conversation.end();
                    }
                })
                // gib alle Fehler aus
                .on('error', (error) => {
                    console.log('Conversation Error:', error);
                });
        };

        // setze den input der Konversation auf den Nachrichtentext
        config.conversation.textQuery = msg.text
        // starte die Konversation
        assistant.start(config.conversation, startConversation)
    })
}

// 'starte' den assistant indem die callbacks registriert werden
assistant
    .on('ready', waitForInput)
    .on('error', (error) => {
        console.log('Assistant Error:', error);
    });