# Demo-Bots

## Über
Dieses Repository beinhaltet vier verschiedene Bots, die unterschiedliche Aspekte der Telegram
Bot-API aufzeigen (Simple Bot, Wetter Bot, TextAdventure Bot, Dialog Bot).
Die Demo-Bots wurden für den Workshop "Bot Programmierung mit Telegram" auf dem Digitalforum 2020 entwickelt.

* **Simple Bot**

    Dies ist ein sehr minimalistischer Bot. Dieser Bot kann Nachrichten wiederholen oder
    Nachrichten rückwärts lesen.


* **Wetter Bot**

    Dieser Bot antwortet auf Standort-Nachrichten mit einer aktuellen Wetterangabe an diesem
    Standort. Es wird die Abfrage einer API, Telegram Keyboards und Standortabfrage demonstriert.


* **TextAdventure Bot**

    Der TextAdventure-Bot nimmt dich mit auf ein interaktives Leseabenteuer. Er nutzt Telegram Keyboards und verschickt
    HTML-Nachrichten und Bilder. Außerdem gibt es ein Dialogflussmodell.


* **Dialog Bot**

    Dieser Bot kann eine sehr einfache Unterhaltung in natürlicher Sprache führen. Dabei wird die Google Assistant API verwendet.
    
## Benutzung
Die Telegram-Bots sind NodeJS-Applikationen, die sich mit diesen Schritten starten lassen:

1. Stelle sicher, dass [NodeJS](https://nodejs.org/) und [npm](https://www.npmjs.com/) installiert sind.
2. Installiere über die Node.js-Komandozeile alle Dependencies (z.B. die Node-Telegram-Bot-Api).

    ```
    npm install
    ```

3. Registriere beim [Telegram-Botfather](https://core.telegram.org/bots#6-botfather) einen Bot. Füge das Token deines Bots in die entsprechende js-Datei ein. Was du beim jeweiligen Bot sonst noch beachten musst, wird in den js-Dateien durch die TODO-Kommentare ausführlich erklärt.

4. Bot starten: Navigiere in der Node.js-Kommandozeile in den Ordner des Bots und starte ihn über:

    ```
    npm start
    ```
    
## Lizenz

Dieses Projekt wurde unter der MIT-Lizenz veröffentlicht. Das Copyright liegt bei Tobias Djuren und Clarissa Staudt.
