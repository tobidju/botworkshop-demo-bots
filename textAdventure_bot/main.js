/*
Mit diesem Telegram-Bot lassen sich interaktive Geschichten gestalten.

Dabei gehst du wie folgt vor:
-   Dieser Bot ist etwas komplizierter aufgebaut. Doch keine Sorge, es ist nicht schwierig eigene Geschichten zu erstellen, denn du musst nicht den gesamten Code verstehen.
    Suche die Zeilen, die einen "TODO"-Kommentar enthalten und passe diese an.
-   In der story.json Datei kannst du deine eigenen Geschichtsbausteine erstellen.
    Dabei musst du ein paar Dinge beachten:
        - Erklärungstext
            Auch wenn gute Bots oft selbsterklärend gestaltet sind, beginnen wir das Gespräch mit einer Erklärung, was der Bot tut.
            Diese befindet sich in einem Element, das unbedingt "Start" heißen muss.
        - Namen
            Jedes Textelement hat einen Namen, z.B. "Start" oder "Einsperren". Jeder Name kann nur einmal vergeben werden.
        - Texte
            Die Geschichtstexte werden in "text" geschrieben. Für Zeilenumbrüche nutzt man \n.
            Am Ende jedes Abschnitts steht die Frage, wie es mit der Geschichte weitergehen soll.
            Dabei wird für jede Option ein Codewort genannt. Dieses Codewort muss mit dem Namen des möglichen Abschnitts identisch sein (z.B. "Angriff").
        - Verbindungen zwischen Geschichtselementen
            Verbindungen zwischen Geschichtselementen werden für jeden Geschichtsabschnitt unter "verbindungen" angegeben.
            Für "Einsperren" heißt das z.B. dass man nach diesem Text zu "Angriff" oder "Ankommen" weitergehen kann.
            Jedes Element außer "Ende" muss mindestens eine Option, in der Geschichte weiterzugehen enthalten.
*/

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*

VORBEREITUNGEN

*/

process.env["NTBA_FIX_319"] = 1

const fs = require('fs'); // Erlaubt den Zugriff auf das Dateisystem, z.B. um Bilder zu laden.
const TelegramBot = require('node-telegram-bot-api') // Stellt die Funktionalität für Telegram-Bots zur Verfügung

const token = '1016657575:AAFK3qTihFAtMV3AsLaTlIETHYnWgLXUFsY' // TODO: Füge hier das Telegram-Token vom @BotFather ein.
const bot = new TelegramBot(token, {polling: true}) // Erstellen eines neuen Telegram-Bots


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*

GRAPH

Wir definieren den Fluss durch die Geschichte als Graphen.
Jedes Geschichtselement stellt einen Knoten dar.
Knoten werden durch Kanten miteinander verbunden.

Beispiel:
Ankommen -> Schlafen | Suchen
Vom Geschichtsabschnitt "Ankommen" kann man zu "Schlafen" oder zu "Suchen" weitergehen.
*/

// Graphenklasse erstellen.
class Graph {
    // Definition des Knoten-Arrays und der Adjazenzliste.
    constructor(){
        this.AdjList = new Map()
    }

    // Neuen Knoten zum Graphen hinzufügem
    neuerKnoten(knoten) {
        this.AdjList.set(knoten, [])
    }

    // Neue Kante zum Graphen hinzufügen
    neueKante(startknoten, endknoten) {
        this.AdjList.get(startknoten).push(endknoten)
    }

    // Knoten und Adjazenzliste ausgeben. Diese Funktionalität eignet sich besonders gut, um Fehler zu finden.
    printGraph() {
        var get_keys = this.AdjList.keys();
        console.log("----------------------------------\n")
        console.log("Geschichtsgraph:\n")
        for (var i of get_keys) {
            var get_values = this.AdjList.get(i)
            let conc = get_values.join(" | ")
            console.log(i + " -> " + conc)

        }
        console.log("\n----------------------------------\n")
    }

    // Findet für einen Knoten alle Optionen mit der Geschichte weiterzumachen.
    findeOptionen(knoten) {
        return(this.AdjList.get(knoten))
    }
}

// Lade die Geschichtsbausteine aus dem Dateisystem.
var story = fs.readFileSync('data/story.json');
story = JSON.parse(story)

// Erstelle einen Graphen aus den Geschichtsbausteinen.
var knoten = Object.keys(story)
var g = new Graph();
for (let i = 0; i < knoten.length; i++) {
    // Knoten hinzufügen.
    let k = knoten[i]
    g.neuerKnoten(k);
    // Kanten hinzufügen.
    let edges = story[k].verbindungen
    for (let j = 0; j < edges.length; j++) {
        g.neueKante(k, edges[j]);
    }
}

// Zeige den erstellten Graphen an.
g.printGraph();



////////////////////////////////////////////////////////////////////////////////

/*

GESCHICHTEN-BOT

*/

var log = [] // Speichert den Zustand aller Chatverläufe

// TODO: Ändere hier die Angaben passend zu deiner Geschichte ab.
var storyDetails = {
    titel: "Die Flucht",
    titelbild: "img/Die_Flucht.jpg",
    autor: "Schokokeks 🍪"
}

// Der Bot reagiert, sobald er eine Nachricht erhält.
bot.on('message', (msg) => {
  const chatId = msg.chat.id; // Sender der Nachricht identifizieren.

  // Beginn einer Geschichte
  if (msg.text == "/start") {

      // Zuerst wird der Text aus "Start" gesendet, der erklärt wie die interaktiven Geschichten funktionieren.
      bot.sendMessage(chatId, story.Start.text, {parse_mode: "HTML"}).then(() => {
          // Dann folgt der Geschichtentitel und Autor

          let dashes = "-----------------------------"
          let geschichtsinfos = "<code>" + dashes + "\n\n<b>" + storyDetails.titel.toUpperCase() + "</b>\n" + "<i>Von " + storyDetails.autor + "</i>\n\n" + dashes + "</code>"

          bot.sendMessage(chatId, geschichtsinfos, {parse_mode: "HTML"}).then(() => {
              // Und das Titelfoto.
              bot.sendPhoto(chatId, storyDetails.titelbild).then(() => {
                  // Im Anschluss wird das erste Geschichtenelement versendet.

                  let nachstesElement = story.Start.verbindungen[0]
                  bot.sendMessage(chatId, story[nachstesElement].text, {
                      parse_mode: "HTML",
                     "reply_markup": {
                         "keyboard": [story[nachstesElement].verbindungen]
                         }
                  });

                  // Wir merken uns, dass wir mit dem Nutzer geschrieben haben und wie weit er in der Geschichte schon ist.
                  log.push({chatId: chatId, aktuellerKnoten: nachstesElement, aktuelleOptionen: g.findeOptionen(nachstesElement)})
              })
          })
      })

  }

  // Wenn keine neue Geschichte gestartet wird, versuchen wir herauszufinden, ob wir den Nutzer schon kennen.
  else {
      let Nutzerinfos = {}
      let NutzerinfosIndex = null
      for (let i = 0; i < log.length; i++) {
          if (log[i].chatId == chatId) {
              Nutzerinfos = {aktuellerKnoten: log[i].aktuellerKnoten, aktuelleOptionen: log[i].aktuelleOptionen}
              NutzerinfosIndex = i
          }
      }

      // Prüfe, ob die Antwort einer der erwarteten Optionen entspricht.
      if (Nutzerinfos.aktuelleOptionen.includes(msg.text)) {

        // Sende das nächste Geschichtselement
        bot.sendMessage(chatId, story[msg.text].text, {
            parse_mode: "HTML",
           "reply_markup": {
               "keyboard": [story[msg.text].verbindungen]
               }
        }).then(() => {
            // Aktualisiere im Log bei welchem Element sich der Nutzer derzeit befindet.
            log[NutzerinfosIndex].aktuellerKnoten = msg.text
            log[NutzerinfosIndex].aktuelleOptionen = g.findeOptionen(msg.text)

        })
      }

      // Wenn die Antwort keine zugelassene Option ist, wird um eine erneute Nachricht gebeten.
      else {
        bot.sendMessage(chatId, "Leider verstehe ich dich nicht. Bitte versuche es noch einmal mit einer der vorgegebenen Antwortmöglichkeiten.", {parse_mode: "HTML"})
      }
  }

})
