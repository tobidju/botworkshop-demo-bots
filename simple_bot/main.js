/* 
Dies ist eine sehr einfache Demo, wie ein Telgram Bot erstellt
werden kann.

Dieser Bot antwortet auf die Telegram-Nachricht/den Telegram Befehl
"\echo irgendwas" mit "irgendwas".
Außerdem schreibt der Bot wiederholt der Bot alle Nachrichten nach dem
Befehl "\reverse" rückwärts. Z.B. "\reverse irgendwas" wird zu "sawdnegri".
"irgendwas" kann natürlich auch ein anderer Text sein.
*/

// diese Codezeile ist ersteinmal nicht weiter wichtig
// sie behebt einen Bug
process.env["NTBA_FIX_319"] = 1;

// importiere die Funktionalität die wir für den Bot brauchen
const TelegramBot = require('node-telegram-bot-api');

// dies ist der token mit dem sich der Bot bei Telegram authentifiziert
const token = 'diesen Text durch eigenen Token vom botfather ersetzen';

// Erstelle einen neuen Bot
const bot = new TelegramBot(token, {polling: true});


// Nun bringen wir dem Bot einen neuen Befehl bei.
// Wenn wir im Chat "/echo irgendwas" schreiben, dann soll der Bot mit
// "irgendwas" antworten. Das "irgendwas" kann dabei auch ein beliebiger anderer
// Text sein
bot.onText(/\/echo (.+)/, (nachricht, match) => { // wenn der Bot einen Text erhält, der mit "/echo " beginnt
  // dann mache alles innerhalb dieser geschweiften Klammer
  // "match" ist ein Objekt, welches das Ergebnis von dem regulären Ausdruck /\/echo (.+)/
  // angewendet auf den Text der Nachricht enthält. Das brauchen wir nicht weiter,
  // nur ein einziges Mal um den Text nach "/echo " herauszufinden
  const text = match[1]; // "text" ist der Text nach "/echo "

  // "nachricht" ist ein Objekt, welches diese Nachricht beschreibt
  // mit dem nachricht-Objekt können z.B. die Id dieses Chats herausgefunden werden:
  const chatId = nachricht.chat.id;

  // sende den Text an den Chat mit der Id chatId zurück
  bot.sendMessage(chatId, text);
});


// der folgende Code bringt dem Bot bei, auf "/reverse" zu antworten
// wenn der Bot den Befehl "/reverse Hallo" erhält, dann soll er mit
// "ollaH" antworten, also die Reihenfolge der Zeichen des Wortes umdrehen
bot.onText(/\/reverse (.+)/, (msg, match) => {

  const chatId = msg.chat.id; // speichere die ID dieses Chats
  var text = match[1]; // text ist die Nachricht nach reverse
  var answerLetters = text.split('') // answerLetters ist ein neuer Array (Liste) der alle Buchstaben des Strings (Zeichenkette) enthält
  answerLetters = answerLetters.reverse() // die Reihenfolge der Buchstaben wird umgedreht
  text = answerLetters.join('') // wandle den Array wieder in einen String um

  // sende die Antwort an den Chat mit der id chatId zurück
  bot.sendMessage(chatId, text);
});