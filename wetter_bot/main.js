/*
Dies ist ein sehr einfacher Bot zur Anzeige des Wetters.
Auf den Befehl "\wetter" schickt der Bot ein personalisiertes Keyboard.
Mit diesem Keyboard kann der/die Nutzer*Inn schnell dein eigenen Standort
freigeben.
Bei Standort-Nachrichten fragt der Bot die OpenWeather-API mit diesem Standort
ab und verschickt eine Beschreibung des aktuellen Wetters an dieser Position.
*/
process.env["NTBA_FIX_319"] = 1;
const TelegramBot = require('node-telegram-bot-api') // die Funktionalität für Telegram-Bots
const wetter = require('openweather-apis') // die Funktionalität für Wetterabfragen

// der Telegram token vom @BotFather
const token = 'mein token'
// der Authentifizierungsschlüssel von OpenWeather
const openWeatherKey = 'mein key von OpenWeather'

// Wir erstellen einen neuen Bot
const bot = new TelegramBot(token, {polling: true})

// Konfigurationen für die Wetter API
wetter.setLang('de') // unsere Sprache ist Deutsch
wetter.setAPPID(openWeatherKey) // Wir geben dem weather unseren Zugangsschlüssel
wetter.setUnits('metric') // Wir wollen alle Angaben in metrischen Einheiten haben



// Wenn der/die Nutzer*in nach dem Wetter fragt, dann soll der Bot nach der Position fragen
bot.onText(/\/wetter/, (msg, match) => {

  const chatId = msg.chat.id;

  const antwort = "Sende mir deine Positon und ich verrate Dir wie das Wetter bei Dir ist!"
  
  // wir erstellen ein Keybord mit dem eine Nutzer*in die Position senden kann
  const button = [{
    text: "Positon senden", // der Text auf dem Button
    request_location: true // mit dem Button wollen wir die Postion abfragen
  }] 
  const keyboard = [button] // unser Keybord besteht nur aus einem Button

  // sende die Antwort und das Keybord
  bot.sendMessage(chatId, antwort, {
    reply_markup: {
      keyboard
    }
  });
});


// wenn der Bot eine Positon erhält, dann fragen wir das Wetter an dieser Positon ab
// und senden eine entsprechende Antwort
bot.on('location', (msg) => {

  const chatId = msg.chat.id
  const location = msg.location // dies ist der gesendete Standort
  // den Standort übergeben wir an unser wetter-Framework
  wetter.setCoordinate(location.latitude, location.longitude)

  bot.sendMessage(chatId, "Ich starte die Wetterabfrage. Einen Moment.")

  // Starte die Wetterabfrage
  // mit .getSmartJSON bekommen wir vereinfachte Wetterdaten in das "daten" Objekt
  // sollte ein Fehler auftreten, stehen Informationen über diesen Fehler im "err" Objekt
  wetter.getSmartJSON((err, daten) => {
    // der Code in dieser Klammer wird ausgeführt, wenn wir vom Wetter-Server eine
    // Antwort erhalten haben
    if(err) console.log(err) // sollte ein Fehler aufgetreten sein geben wir diesen auf der Console aus

    // generiere und sende eine Antwort
    var antwort = `An deiner Positon ist es ${daten.temp}° warm und das Wetter ist: ${daten.description}`
    bot.sendMessage(chatId, antwort)
  })
})